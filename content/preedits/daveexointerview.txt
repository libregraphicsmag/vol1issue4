Exo: Solving a problem for all designers
by Natanael Gama and Dave Crossland

Natanael approached the Google Web Fonts team in October to let them know that he had been working on a big type project for two years: Exo. The project was coming to an end and he was considering how to make it available as widely as possible. He knew from a friend who was already publishing fonts through Google Web Fonts that financial support is available for type designers who wish to make their type designs libre, so he got in touch with an impressive PDF specimen. We decided to run Exo as a Kickstarter project!

Dave Crossland: So Natanael, what is your background as a designer, and how did you become interested in type design?

Natanael Gama: When I was a teenager I become fascinated by graffiti and the way you could manipulate letter shapes. Since then I have been interested in all kinds of lettering art, but my interest became more serious when I was at University where I took typography classes.

I was attracted by the way beauty and functionality need to be perfectly balanced to create a great typeface. As a student I was searching for a area in graphic design where a project could last for long time and eventually be remembered. I felt graphic design would usually be disposable, sometimes made with little effort and thinking, and when I looked to type design I saw the opposite. That attracted me.

DC Are there any type designers who inspire you?

NG Dino dos Santos, Ricardo Santos, Mário Feliciano and Rui Abreu, all from the Portuguese type scene, because they made Portuguese type design reach a different level.

Also Erik Spiekermann, Sebastian Lester and Eduardo Manso are references to me.

DC What is your favourite part of the type design process, and why?

NG My favorite part is the design of the glyphs at the very first stage. When ideas turn into something tangible, even if they start as little sketches. I think this is where creativity plays a very important role. And for sure, without creativity there is no type design.

DC Designing a new typeface is a long journey. What inspires you to keep motivated throughout all the different stages?

NG It is indeed a long journey, and its always longer than you thought it would be! I must say that sometimes a person might get overwhelmed with the amount of work and a little bit discouraged by not knowing if all the work will be worth anything in the end.

But as you see the work being done, you get courage to do more, and with the motivation of your friends the doubts start to fade away. When I start, I imagine that people will actually use my work, and I get motivated to complete it that way.

DC Can you recommend how other type designers can learn the skills involved in making type?

NG I think besides a general graphc design education, you must be personally involved in the type world; taking a look at new font releases, taking some time to appreciate the great classic types, to examine the typography being used around you in daily life, reading articles and books. Being interested!

While designing you will understand that you have gaps in your knowledge, you can post your questions on typography forums, like Typophile, and ask more experienced people about things. It is really good to personally know other type designers who you can talk with.

DC What do you think could be improved about the type design process?

NG I think the type design process takes a lot of time because we don't have software to do some things for us. I am sure some designers will disagree with me, but for instance, you take a lot of time perfecting your kerning, to get optically pleasant spaces between letters... and I'm sure some algorithm can do that faster and better. Of course, you must control your software to meet your needs.... and kerning is just one aspect, there are more.  

DC Turning to your Exo project, could you describe it?

NG Exo is a contemporary geometric sans font family with a sleek, technological look. It has a very peculiar look, but it can be very versatile. It is a very complete font family, with over 700 characters supporting most Western, Central and Eastern European languages - and it can still go further!

This coverage has been reached for all 9 weights, the maximum possible on the web. Each weight has an upright style and the corresponding ‘true’ italic style. Many ‘techno’ fonts have only the simple slanted oblique companion styles, but for the highest typographic quality it is essential to have true italics that are drawn specifically.

The OpenType features include small caps, standard and discretionary ligatures, stylistic alternates, old style figures, tabular figures, fractions, and more. These features will be available both as true OpenType fonts and also as ‘simple’ fonts for use on the web.

DC Do you feel you achieved what you wanted from the design?

NG I feel I did, but I think in the end, only the users can say if a type designer did succeed or not.

I designed it for use in magazines and magazine-style websites, advertising. I made it for display use, and it can work to typeset small amounts of texts. It could even work well as a corporate identity typeface for the right company.

DC It is an impressive project. What is the story behind it?

NG When I started my career as a freelance graphic designer I faced the problem of not having enough good fonts to do decent work. When you are starting out, you don’t have that much money to invest in good typography, so many people break the license agreements of the fonts they use. Since I wanted to be as legal as possible, I got into the world of libre fonts - and I am so thankful to those designers who are generous enough to share their work with the world!

But along the way I have found that right now there are very few libre fonts you can count on. This was a problem for me, and for sure it is the same for all those serious designers who avoid pirate fonts. The cheap stuff very often lacks important features to do a good job, but it can’t be changed or fixed. The amazing thing about libre fonts is that they are both available at zero price and can also be improved, like Wikipedia!

Having this situation in mind, I decided to invest my own time into the design of a good font family to help all designers. So I made Exo for myself and designers like me - a font family for those who want to do a good job, but may lack the fonts to do it properly. I decided to make this a libre font so the designers can do great work with a legal font family, and learn the value of high quality type.

I spoke to the Google Web Fonts team about publishing Exo with them, they gave my project their full support. They suggested that I should llicense it under the SIL Open Font License (http://scripts.sil.org/OFL) and also that I try to fund the project with Kickstarter.

Google Web Fonts was just starting an experiment with Kickstarter, and normally I wouldn’t be able to use that site because it uses Amazon Payments, which will only pay out to bank accounts in the USA. But Google Web Fonts had an arrangement with the USA TeX Users Group, a US based 501(c)3 non-profit, who are running a ‘Libre Font Fund.’ This allowed me to receive the funds outside the USA here in Portugal.

DC How do you feel about the KickStarter project?

Initially I thoughts about a US$15,000 goal. Since Kickstarter projects require the backer community to reach the goal, or it doesn’t happen, and often the goals are overreached, I decided to set it much lower - at $7,500.  I set the time for about 50 days, and it has only been running for a week and already about $1,500 is pledged.

I don’t mean to call on all designers to stop buying and selling proprietary fonts, I myself buy proprietary font licenses and think that you should return value to such wonderful work as type! It seemed that Kickstarter would be a great way to do both things - to sell the work and for the users of type to give something back to the type designer, while at the same time eventually making the font available to the whole world. 
