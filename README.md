# The Physical, the Digital and the Designer, Issue 1.4

## Index

* **Editor's letter**, ginger coon — *Editor's letter*
* **Branching out: journey notes**, Manufactura Independente — *Production colophon*
* *New Releases*
* *Upcoming events*
* **Moral Rights and the SIL Open Font License**, Dave Crossland — *Column*
* **Will these hands ever be dirty**, Eric Schrijver — *Column*
* **Cultura Digital Festival** — *Notebook*
* *Small and Useful*
* **The finished and unfinished business of OpenLab ESEV**, Nelson Gonçalves and Maria Figueiredo — *Dispatch*
* **Maps** — *Best of SVG*
* **Natanael Gama talks tecno-fonts and the benefits of Libre**, Natanael Gama interviewed by Dave Crossland — *Interview*
* **The graphic side of 3D printing**, Thingiverse — *Showcase*
* **Paper.js:building, designing and the browser**, Jonathan Puckey (interviewed by Manufactura Independente) — *Showcase*
* **The Baltan Cutter or the thin line between intuitive and generative**, Eric de Haas — *Showcase*
* **Folds, impositions and gores: an interview with Tom Lechner**, Tom Lechner interviewed by Femke Snelting, Pierre Marchand and Ludivine Loiseau — *Interview*
* **ColorHug: filling the F/LOSS colour calibration void**, Richard Hughes — *Feature*
* **Ressurecting the noble plotter**, Manufactura Independente and Diogo Tudela — *Feature*
* *Resource list*
* *Glossary*

## Colophon

The Physical, the Digital and the Designer — March 2012  
Issue 1.4, [Libre Graphics Magazine](http://libregraphicsmag.com)  
ISSN: 1925-1416

**Editorial Team:**  
* [Ana Isabel Carvalho](http://manufacturaindependente.org)
* [ginger coons](http://adaptstudio.ca)
* [Ricardo Lafuente](http://manufacturaindependente.org)