3D printing cool - F/LOSS digital, F/LOSS physical

airplane cookie cutters (inkscape) - user: emmett on Thingiverse (CC BY SA)
http://www.thingiverse.com/thing:11098

I heart lightning (Inkscape & OpenSCAD) - Amy Hurst (CC BY)
http://www.thingiverse.com/thing:17495

DNA Playset (OpenSCAD) - Emmett Lalish (CC BY SA)
http://www.thingiverse.com/thing:17343

Lake and mountain topography (Blender) - Luke Chilson (CC BY SA)
http://www.thingiverse.com/thing:7207


